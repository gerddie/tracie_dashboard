#!/bin/sh
set -x

echo "$@"
OPTS=$(getopt -o p:i: -l host-port:,docker-image: -n 'start-docker.sh' -- "$@")

echo "$OPTS"
eval set -- "$OPTS"

HOST_PORT=80
DOCKER_IMAGE="tracie_dashboard:latest"

while true; do
    case "$1" in
        -p|--host-port) HOST_PORT="$2"; shift; shift; ;;
        -i|--docker-image) DOCKER_IMAGE="$2"; shift; shift; ;;
        --) shift; break; ;;
        *) break; ;;
    esac
done

# Change to script's directory
cd "$(dirname "$(readlink -f "$0")")"

. ./setup-env.sh

docker run --rm -p $HOST_PORT:80 \
        -e DJANGO_SECRET_KEY="$DJANGO_SECRET_KEY" \
        -e DJANGO_ALLOWED_HOST="$DJANGO_ALLOWED_HOST" \
        -e DJANGO_GITLAB_TOKEN="$DJANGO_GITLAB_TOKEN" \
        -e DJANGO_DEBUG="$DJANGO_DEBUG" \
        --name tracie_dashboard \
        "$DOCKER_IMAGE"
