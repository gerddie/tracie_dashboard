from django.db import models

class PipelineResult(models.Model):
    id = models.IntegerField(primary_key=True)
    url = models.URLField()
    mesa_sha = models.CharField(max_length=40, null=True)
    mesa_url = models.URLField(null=True)
    status = models.CharField(max_length=10)
    test_job_id = models.IntegerField(null=True)
    triggered_by_mesa_push = models.BooleanField()

class TraceResult(models.Model):
    summary_url = models.URLField()
    status = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    pipeline_result = models.ForeignKey(PipelineResult, on_delete=models.CASCADE)

