from django.urls import path

from . import views

urlpatterns = [
    path('', views.landing, name='landing'),
    path('pipeline/<int:pid>/', views.results, name='results'),
]
